package lab01.example.model;

//decorator pattern
public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithATM {

    private final BankAccount bankAccount;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
        bankAccount = new SimpleBankAccount(holder, balance);
    }

    @Override
    public void deposit(int usrID, double amount) {
        this.bankAccount.deposit(usrID, amount - COMMIT_FEE);
    }

    @Override
    public void withdraw(int usrID, double amount) {
        this.bankAccount.withdraw(usrID, amount + COMMIT_FEE);
    }

    @Override
    public double getBalance() {
        return bankAccount.getBalance();
    }

    //template method
    @Override
    protected boolean isWithdrawAllowed(double amount) {
        return super.isWithdrawAllowed(amount - COMMIT_FEE);
    }
}
